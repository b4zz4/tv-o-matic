# Presentación al instante

¿No tentes tiempo para hacerlo?, ¿No tenes un locutor?...
Presentaciones al instante con **tv-o-matic**

## Concepto

![ejemplo](ejemplo.png)
> Generar vídeo a partir de algunas imágenes y un texto

## Instalar

`sudo apt-get install mbrola mbrola-es2 avconv imagemagick mpv`

### Proyecto

* **texto.txt** contiene el texto
* **imagenes/** contiene las imágenes (png o jpg) para generar el vídeo

#### Generar el vídeo

`./generar.sh`

**¡¡Listo!!**

## Quehaceres

* quitar mpv
* Poner texto en el vídeo y/o srt
* Generar transiciones

--- 

Bitcoin: 19qkh5dNVxL58o5hh6hLsK64PwEtEXVHXs
