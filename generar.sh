#!/bin/bash
#
#	genera videos a partir de imagenes y un texto
#

# por las dudas
rm -r tmp 2> /dev/null
mkdir tmp 2> /dev/null

AUDIO=tmp/audio.wav

cat texto.txt | sed "s/\./\.\n/g; s/á/'a/g; s/é/'e/g; s/í/'i/g; s/ó/'o/g; s/ú/'u/g;" | ./ttp > $AUDIO

# tiempo en segundos (quitar la dependencia de mplayer)
if [ ! -x $AUDIO ]; then
	# usar avconv
	TIEMPO="$(echo "$(mpv -vo null -ao null -frames 0 -identify $AUDIO | grep "ID_LENGTH=" | sed 's/^.*=//g;')*25"|bc -l)"
fi

# entra a la carpera de imagenes
cd imagenes

# cuenta la cantidad de imagenes
CANTIDAD=$(echo "$TIEMPO/$(ls | wc -l)" | bc -l)
rename 's/\ /_/g' * # esta truchada hay que sacarla

echo $CANTIDAD $TIEMPO

# pone la imagen al tamaño de salida (falta solo imagenes)
ls | while read I; do convert $I -resize 500x375 -fill white \
-gravity South   -background black -splice 0x20 -annotate 	+0+5 ' Canal hacker ' ../tmp/$I; done
# http://www.imagemagick.org/Usage/annotating/
cd ../tmp

N=0
# une las imagenes
for I in $(ls | grep "\.\(png\|jpg\)$"); do
	for X in $(seq 1 $CANTIDAD); do
		# hace la cantidad necesarias de copias
		echo "$I - $X" > /dev/stderr
		cp $I $(printf "%03d" ${N}).png
		N=$(($N+1))
	done
done
cd ..

# genera el video final
avconv -i tmp/%03d.png -i $AUDIO -r 25  -c:v libvpx -minrate 2M -maxrate 4M -b:v 1M -c:a libvorbis video.webm

# borra los temporales
rm -r tmp 2> /dev/null

exit

# Quehacers
- solo tenes de dependencia avconv y mbrola
- elegir solo entre mplayer y mpv
